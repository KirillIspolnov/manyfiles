package net.kivitechnologies.BrainWork.ManyFiles2;

public class Ticket {
    private int[] digits;
    
    public Ticket(int number) {
        digits = new int[6];
        for(int i = 0; i < 6; i++) {
            digits[5 - i] = (int)((number % Math.pow(10, i + 1)) / Math.pow(10, i));
        }
    }
    
    public boolean isSixDigit() {
        return digits[0] != 0;
    }
    
    public boolean isLucky() {
        return digits[0] + digits[1] + digits[2] == digits[3] + digits[4] + digits[5];
    }
    
    public int toInt() {
        int number = 0;
        for(int i = 0; i < 6; i++) {
            number += digits[i] * Math.pow(10, 5 - i);
        }
        return number;
    }
}
