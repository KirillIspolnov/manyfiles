package net.kivitechnologies.BrainWork.ManyFiles;

import java.util.Scanner;

/**
 * ВНИМАНИЕ! Данный класс не несёт в себе цели коим образом оскорбить чувтсва верующих! (Я сам верующий)
 * В данном конкретно случае необходимо переводить только первую часть названия - Number (число)
 * Спасибо за внимание!
 * 
 * Содержит методы для получения массива чисел: генерация псевдослучайных чисел и получение их от пользователя, 
 * а также для определения удачности числа
 * 
 * @author Кирилл Испольнов, 16ит18К
 */
public class NumberGod {
    /**
     * Кол-во чисел
     */
    public static final int COUNT = 1_000;
    
    /**
     * Генерирует COUNT псевдослучайных чисел в диапазоне от 100000 до 1000000
     * 
     * @return массив псевдослучайных чисел в диапазоне от 100000 до 1000000
     */
    public static int[] generate() {
        int[] numbers = new int[COUNT];
        for(int i = 0; i < COUNT; i++) {
            numbers[i] = 1_000 + (int)(Math.random() * 1_000_000);
        }
        return numbers;
    }
    
    /**
     * Получает COUNT чисел от пользователя. Не стыда у Вас ни совести! Хотите заставить юзера вводить 1000 чисел?
     * Злодейство чистой воды! Ай-яй-яй!
     * 
     * @return массив чисел, полученных от пользователя
     */
    public static int[] prompt() {
        int[] numbers = new int[COUNT];
        Scanner scanner = new Scanner(System.in);
        for(int i = 0; i < COUNT; i++) {
            numbers[i] = scanner.nextInt();
        }
        scanner.close();
        return numbers;
    }
    
    /**
     * Проверяет ШЕСТИЗНАЧНОЕ число на удачность
     * Подадите не шестизначное число будет Double-Trouble!
     * 
     * @param number ШЕСТИЗНАЧНОЕ число для проверки на удачность
     * @return true, если сумма первых трёх цифр равна сумме трёх последних
     */
    public static boolean isLucky(int number) {
        int[] digits = new int[]{
                number / 100_000,
                number / 10_000 % 10,
                number / 1_000 % 10,
                number / 100 % 10,
                number / 10 % 10,
                number % 10
        };
        
        return digits[0] + digits[1] + digits[2] == digits[3] + digits[4] + digits[5];
    }
}
