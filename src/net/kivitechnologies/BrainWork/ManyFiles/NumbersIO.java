package net.kivitechnologies.BrainWork.ManyFiles;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Содержит методы для записи и чтения чисел с помощью файлов
 * 
 * @author Кирилл Испольнов, 16ит18К
 */
public class NumbersIO {
    /**
     * Конвертирует целое число (int, 32-bit) в массив из 4-х байт
     * Используется для представления числа в подлежащей дальнейшей записи форме
     * 
     * @param number 32-битное число
     * @return массив из 4 байт описывающих целое 32-битное цисло
     */
    private static byte[] intToBytes(int number) {
        return new byte[]{
            (byte)(number >>> 24),
            (byte)(number >>> 16),
            (byte)(number >>> 8 ),
            (byte)(number)
        };
    }
    
    /**
     * Конвертирует массив из 4-х байт в целое 32-битное число
     * Используется для предстваления числа в привычной для программы форме int
     * 
     * @param bytes  массив из 4 байт описывающих целое 32-битное цисло
     * @return 32-битное число
     */
    private static int bytesToInt(byte[] bytes) {
        return ((bytes[0] & 0xFF) << 24) | ((bytes[1] & 0xFF) << 16) | ((bytes[2] & 0xFF) << 8) | (bytes[3] & 0xFF);
    }
    
    /**
     * Записывает массив чисел numbers с помощью представления каждого числа как массив байт 
     * и последующей записи последовательности байт в файл с помощью {@link java.io.FileOutputStream}  
     * Мой слегка велосипедный метод ЫЫЫЫЫЫ
     * 
     * @param numbers массив целых чисел для записи в файл
     */
    public static void writeByBicycle(int[] numbers) {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream("numbers.dat");
            
            for(int number : numbers) {
                fos.write(intToBytes(number));
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        
        try {
            fos.close();
        } catch (IOException | NullPointerException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Записывает массив чисел numbers с помощью {@link java.io.DataOutputStream}  
     * 
     * @param numbers массив целых чисел для записи в файл
     */
    public static void writeByDOS(int[] numbers) {
        DataOutputStream dos = null;
        try {
            dos = new DataOutputStream(new FileOutputStream("numbers.dat"));
            
            for(int number : numbers) {
                dos.writeInt(number);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        
        try {
            dos.close();
        } catch (IOException | NullPointerException ex) {
            ex.printStackTrace();
        }   
    }
    
    /**
     * Получает массив чисел numbers
     * Считывает с помощью {@link java.io.FileInputStream} 4 байта информации 
     * А затем преобразует полученный массив байт в целое число и сохраняет его в результативный массив  
     * 
     * @return numbers массив целых чисел, считанных из файла
     */
    public static int[] readByBicycle() {
        FileInputStream fis = null;
        int[] numbers = null;
        
        try {
            File file = new File("numbers.dat");
            fis = new FileInputStream(file);
            numbers = new int[(int)file.length() / 4];
            byte[] bytes = new byte[4];
            
            for(int i = 0; i < numbers.length; i++) {
                fis.read(bytes);
                numbers[i] = bytesToInt(bytes);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        
        try {
            fis.close();
        } catch (IOException | NullPointerException ex) {
            ex.printStackTrace();
        }
        
        return numbers;
    }
    
    /**
     * Получает массив чисел numbers
     * Считывает с помощью {@link java.io.DataInputStream} каждое число, а затем сохраняет его в массив, подлежащий возврату из метода
     * 
     * @return numbers массив целых чисел, считанных из файла
     */
    public static int[] readByDIS() {
        DataInputStream dis = null;
        int[] numbers = null;
        
        try {
            File file = new File("numbers.dat");
            dis = new DataInputStream(new FileInputStream(file));
            numbers = new int[(int)file.length() / 4];
            
            for(int i = 0; i < numbers.length; i++) {
                numbers[i] = dis.readInt();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        
        try {
            dis.close();
        } catch (IOException | NullPointerException ex) {
            ex.printStackTrace();
        }
        
        return numbers;
    }
}
