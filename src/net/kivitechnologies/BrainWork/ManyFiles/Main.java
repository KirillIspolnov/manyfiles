package net.kivitechnologies.BrainWork.ManyFiles;

import java.io.FileWriter;
import java.io.IOException;

/**
 * БЭГКГКГГКГРГФ
 * УИИИИИИИ
 * Основной класс, что я могу еще скзаать
 * 
 * @author Кирилл Испольнов, 16ит18К
 */
public class Main {

    /**
     * А вот интересно, как JVM вызывает именно этот метод? Что-то вроде рефлексии, да? Если так, то довольно медленный способ...
     * 
     * @param args массив строковых аругментов, переданных программе
     * @throws IOException
     */
    public static void main(String[] args) {
        int[] numbers = NumberGod.generate();
        NumbersIO.writeByBicycle(numbers);
        numbers = NumbersIO.readByBicycle();
        
        FileWriter fw = null;
        try {
            fw = new FileWriter("results.txt");
            for(int number : numbers) {
                System.err.println(number);
                if(number < 100_001 || number > 999_999 || !NumberGod.isLucky(number))
                    continue;
                
                fw.write(number + "\n");
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        
        try {
            fw.close();
        } catch (IOException | NullPointerException ex) {
            ex.printStackTrace();
        }
    }

}
